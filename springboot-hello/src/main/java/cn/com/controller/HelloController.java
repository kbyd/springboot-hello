package cn.com.controller;

import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.domain.Demo;

@RestController
public class HelloController {

	@RequestMapping("/hello")
	public String  hello() {
		return "hello-2017";
	}
	
	@RequestMapping("/getDemo")
	public Demo getDemo() {
		Demo demo = new Demo();
		demo.setId("1");
		demo.setName("力王");
		demo.setDate(new Date());
		return demo;
	}

}
